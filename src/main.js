// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import * as moment from 'moment';
// let moment = require("moment");
// if ("default" in moment) {
//     moment = moment["default"];
// }
import VueMomentJS from 'vue-momentjs';
//const moment = require('moment');
import axios from 'axios';
import VueAxios from 'vue-axios';

Vue.use(VueAxios, axios);
//Vue.use(require('vue-moment'), { moment});
Vue.use(VueMomentJS, moment);

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  template: '<App/>',
  components: { App }
})
